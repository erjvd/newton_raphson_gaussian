import numpy as np
import time
import sys
import shutil
import copy
import math
import subprocess
from scipy import linalg
from scipy import optimize

### all input parameters ###
project_name='test'
template='temp.gjf'
coord_file='ethane_init.xyz'
trust_radius=0.01        #0.1 angstrom in J. Phys. Chem. A, Vol. 113, No. 43, 2009

#--------------------------------------------------------------------------------------------------------------------------------#
#                                                                                                                                #
#    All functions to read output files are below.                                                                               #
#                                                                                                                                #
#--------------------------------------------------------------------------------------------------------------------------------#

def read_hessian():
    # read the Hessian from Gaussian
    indices=getLowerTriangularIndices(3*Natom)
    with open('hessian.out', "r") as inputfile:
        read=False
        Hessian=np.zeros((3*Natom, 3*Natom))
        j=0
        for line in inputfile:
            if line.startswith(' Data'):
                read=True
            elif line.startswith(' Enter'):
                read=False
            elif read:
                for i in range(len(line.split())):
                    Hessian[indices[0][j+i]][indices[1][j+i]]=float(line.split()[i].replace('D', 'E'))
                    Hessian[indices[1][j+i]][indices[0][j+i]]=float(line.split()[i].replace('D', 'E'))
                j+=4
    return(Hessian)

def read_xyz_file():
    # read a xyz file.
    coord=[]
    atom = np.loadtxt(coord_file, dtype='str', unpack=True, skiprows=2, usecols=0).tolist()
    x, y, z = np.loadtxt(coord_file, dtype=float, unpack=True, skiprows=2, usecols=(1,2,3))
    for i in range(len(x)):
        coord.extend([x[i]/0.529177210903, y[i]/0.529177210903, z[i]/0.529177210903])
    return(atom, coord)

def read_Gaussian_out():
    # read the energies and gradients from gaussian
    force=[]
    with open(project_name+'.log', "r") as inputfile:
        read=False
        for line in inputfile:
            if 'Forces (Hartrees/Bohr)' in line:
                read=True
            elif 'Cartesian Forces' in line:
                read=False
            elif read and line.split()[0].isdigit():
                force.append(-float(line.split()[2]))
                force.append(-float(line.split()[3]))
                force.append(-float(line.split()[4]))
            elif 'SCF Done:' in line:
                energy=float(line.split()[4])
            elif 'Total Energy, E(TD-HF/TD-DFT)' in line:
                energy=float(line.split()[-1])
    return(energy, force)


#--------------------------------------------------------------------------------------------------------------------------------#
#                                                                                                                                #
#    All functions to write files are below.                                                                                     #
#                                                                                                                                #
#--------------------------------------------------------------------------------------------------------------------------------#

def prepare_g16_input(coord):
    # prepare the Gaussian input
    with open(project_name+'.gjf', 'w') as inputfile1:
        with open(template, 'r') as refile:
            for line in refile:
                inputfile1.write(line)
        for i in range(len(atom)):
            inputfile1.write("%s %25.15f %25.15f %25.15f \n" %(atom[i].capitalize(), coord[i*3]*0.529177210903, coord[i*3+1]*0.529177210903, coord[i*3+2]*0.529177210903))
        inputfile1.write('\n')

def write_coord_angstrom(atom, coord, name):
    # write the coordinates in angstrom
    with open(name, 'w') as outputfile:
        outputfile.writelines(str(len(atom)) + '\n coord.xyz \n')
        for i in range(len(atom)):
            outputfile.write('%s %15.10f %15.10f %15.10f %s' %(atom[i].title(), coord[i*3]*0.529177210903, coord[i*3+1]*0.529177210903, coord[i*3+2]*0.529177210903, '\n' ))

def write_all_coord():
    # append the coordinates to create a trajectory file
    with open('all_coord.xyz', 'a') as coordallfile:
        with open('coord.xyz', 'r') as coordfile:
            coordallfile.write(coordfile.read())

def write_iterfile():
    # write the iteration and energy to a file
    with open('iter.log', 'a') as iterfile:
        iterfile.write('%d %15.10f %s' %(iteration, energy, '\n' ))

#--------------------------------------------------------------------------------------------------------------------------------#
#                                                                                                                                #
#    All functions to calculate values are below.                                                                                #
#                                                                                                                                #
#--------------------------------------------------------------------------------------------------------------------------------#

def run_gaussian():
    # run Gaussian 16
    print('running Gaussian')
    subprocess.run(['g16',project_name+'.gjf'])           # maybe parallelise
    print('finished running Gaussian')
    subprocess.run(['formchk',project_name+'.chk', project_name+'.fchk'])
    subprocess.call("demofc < demofc.inp > hessian.out", shell=True)

def getLowerTriangularIndices(n):
    return [list(x) for x in np.tril_indices(n)]

def get_masses():
    # get the masses
    mass = []
    temp = []
    MM_of_Elements = {'H': 1.007825, 'He': 4.002602, 'Li': 6.941, 'Be': 9.012182, 'B': 10.811, 'C': 12.0,
    'N': 14.0030740,'O': 15.9949146, 'F': 18.9984032, 'Ne': 20.1797, 'Na': 22.98976928, 'Mg': 24.305,
    'Al': 26.9815386,'Si': 28.0855, 'P': 30.973762, 'S': 32.065, 'Cl': 35.453, 'Ar': 39.948, 'K': 39.0983,
    'Ca': 40.078,'Sc': 44.955912, 'Ti': 47.867, 'V': 50.9415, 'Cr': 51.9961, 'Mn': 54.938045,'Fe': 55.845,
    'Co': 58.933195, 'Ni': 58.6934, 'Cu': 63.546, 'Zn': 65.409, 'Ga': 69.723, 'Ge': 72.64,'As': 74.9216,
    'Se': 78.96, 'Br': 79.904, 'Kr': 83.798, 'Rb': 85.4678, 'Sr': 87.62, 'Y': 88.90585,'Zr': 91.224,
    'Nb': 92.90638, 'Mo': 95.94, 'Tc': 98.9063, 'Ru': 101.07, 'Rh': 102.9055, 'Pd': 106.42,'Ag': 107.8682,
    'Cd': 112.411, 'In': 114.818, 'Sn': 118.71, 'Sb': 121.760, 'Te': 127.6,'I': 126.90447, 'Xe': 131.293,
    'Cs': 132.9054519, 'Ba': 137.327, 'La': 138.90547, 'Ce': 140.116,'Pr': 140.90465, 'Nd': 144.242,
    'Pm': 146.9151, 'Sm': 150.36, 'Eu': 151.964, 'Gd': 157.25,'Tb': 158.92535, 'Dy': 162.5, 'Ho': 164.93032,
    'Er': 167.259, 'Tm': 168.93421, 'Yb': 173.04,'Lu': 174.967, 'Hf': 178.49, 'Ta': 180.9479, 'W': 183.84,
    'Re': 186.207, 'Os': 190.23, 'Ir': 192.217,'Pt': 195.084, 'Au': 196.966569, 'Hg': 200.59, 'Tl': 204.3833,
    'Pb': 207.2, 'Bi': 208.9804,'Po': 208.9824, 'At': 209.9871, 'Rn': 222.0176, 'Fr': 223.0197, 'Ra': 226.0254,
    'Ac': 227.0278,'Th': 232.03806, 'Pa': 231.03588, 'U': 238.02891, 'Np': 237.0482, 'Pu': 244.0642,
    'Am': 243.0614,'Cm': 247.0703, 'Bk': 247.0703, 'Cf': 251.0796, 'Es': 252.0829, 'Fm': 257.0951,
    'Md': 258.0951,'No': 259.1009, 'Lr': 262, 'Rf': 267, 'Db': 268, 'Sg': 271, 'Bh': 270, 'Hs': 269,
    'Mt': 278,'Ds': 281, 'Rg': 281, 'Cn': 285, 'Nh': 284, 'Fl': 289, 'Mc': 289, 'Lv': 292, 'Ts': 294, 'Og': 294}
    for element in atom:
        try:
            mass.append(MM_of_Elements[element.capitalize()])
        except:
            sys.exit('The mass of element ', element.capitalize(), 'was not found.')
    for x in mass:
        temp.extend(np.repeat(math.sqrt(x*1822.888486209), 3).tolist()) # *3 for x, y and z and conversion from amu to atomic units [m_e]
    massmatrix=np.diag(temp) # make a diagonal massmatrix
    mass=np.multiply(mass, 1822.888486209).tolist()
    return(mass, massmatrix)

def center_molecule(coord):
    # move the center of mass to the origin.
    tot_mass=0
    mm_coord=[0, 0, 0]
    MM_of_Elements = {'H': 1.00794, 'He': 4.002602, 'Li': 6.941, 'Be': 9.012182, 'B': 10.811, 'C': 12.0107,
            'N': 14.0067,'O': 15.9994, 'F': 18.9984032, 'Ne': 20.1797, 'Na': 22.98976928, 'Mg': 24.305,
            'Al': 26.9815386,'Si': 28.0855, 'P': 30.973762, 'S': 32.065, 'Cl': 35.453, 'Ar': 39.948, 'K': 39.0983,
            'Ca': 40.078,'Sc': 44.955912, 'Ti': 47.867, 'V': 50.9415, 'Cr': 51.9961, 'Mn': 54.938045,'Fe': 55.845,
            'Co': 58.933195, 'Ni': 58.6934, 'Cu': 63.546, 'Zn': 65.409, 'Ga': 69.723, 'Ge': 72.64,'As': 74.9216,
            'Se': 78.96, 'Br': 79.904, 'Kr': 83.798, 'Rb': 85.4678, 'Sr': 87.62, 'Y': 88.90585,'Zr': 91.224,
            'Nb': 92.90638, 'Mo': 95.94, 'Tc': 98.9063, 'Ru': 101.07, 'Rh': 102.9055, 'Pd': 106.42,'Ag': 107.8682,
            'Cd': 112.411, 'In': 114.818, 'Sn': 118.71, 'Sb': 121.760, 'Te': 127.6,'I': 126.90447, 'Xe': 131.293,
            'Cs': 132.9054519, 'Ba': 137.327, 'La': 138.90547, 'Ce': 140.116,'Pr': 140.90465, 'Nd': 144.242,
            'Pm': 146.9151, 'Sm': 150.36, 'Eu': 151.964, 'Gd': 157.25,'Tb': 158.92535, 'Dy': 162.5, 'Ho': 164.93032,
            'Er': 167.259, 'Tm': 168.93421, 'Yb': 173.04,'Lu': 174.967, 'Hf': 178.49, 'Ta': 180.9479, 'W': 183.84,
            'Re': 186.207, 'Os': 190.23, 'Ir': 192.217,'Pt': 195.084, 'Au': 196.966569, 'Hg': 200.59, 'Tl': 204.3833,
            'Pb': 207.2, 'Bi': 208.9804,'Po': 208.9824, 'At': 209.9871, 'Rn': 222.0176, 'Fr': 223.0197, 'Ra': 226.0254,
            'Ac': 227.0278,'Th': 232.03806, 'Pa': 231.03588, 'U': 238.02891, 'Np': 237.0482, 'Pu': 244.0642,
            'Am': 243.0614,'Cm': 247.0703, 'Bk': 247.0703, 'Cf': 251.0796, 'Es': 252.0829, 'Fm': 257.0951,
            'Md': 258.0951,'No': 259.1009, 'Lr': 262, 'Rf': 267, 'Db': 268, 'Sg': 271, 'Bh': 270, 'Hs': 269,
            'Mt': 278,'Ds': 281, 'Rg': 281, 'Cn': 285, 'Nh': 284, 'Fl': 289, 'Mc': 289, 'Lv': 292, 'Ts': 294, 'Og': 294}
    for num, element in enumerate(atom):
        try:
            tot_mass += MM_of_Elements[element.capitalize()]
            mm_coord[0]+= MM_of_Elements[element.capitalize()]*coord[num*3]
            mm_coord[1]+= MM_of_Elements[element.capitalize()]*coord[num*3+1]
            mm_coord[2]+= MM_of_Elements[element.capitalize()]*coord[num*3+2]
        except:
            sys.exit('The mass of element ', element.capitalize(), 'was not found.')
    coord_origin = np.subtract(coord, [mm_coord[0]/tot_mass,mm_coord[1]/tot_mass,mm_coord[2]/tot_mass]*len(atom)).tolist()
    return(coord_origin)

def Tm(angles):
    m1=[[math.cos(angles[0]), math.sin(angles[0]), 0],[-math.sin(angles[0]), math.cos(angles[0]), 0],[0, 0, 1]]
    m2=[[math.cos(angles[1]), 0, -math.sin(angles[1])],[0, 1, 0],[math.sin(angles[1]), 0, math.cos(angles[1])]]
    m3=[[1, 0, 0],[0, math.cos(angles[2]), math.sin(angles[2])],[0, -math.sin(angles[2]), math.cos(angles[2])]]
    ec=np.dot(m1,np.dot(m2,m3))
    return(ec)

def eckart_funct(angles, coord1, coord2,mass):
    # Eckart function to overlap coordinates
    ec=Tm(angles)
    expr=0
    for i in range(int(len(coord1)/3)):
        expr +=np.cross(coord1[i*3:i*3+3], np.dot(ec,coord2[i*3:i*3+3]))*mass[i]
    return(expr)

def solve_eckart(coord1, coord2,mass):
    sol = optimize.root(eckart_funct, [0.1, 0.1, 0.1,], args=(coord1, coord2,mass), method='krylov')
    ec=Tm(sol.x)
    coord_eck=[]
    for i in range(int(len(coord2)/3)):
        coord_eck.extend(np.dot(ec,coord2[i*3:i*3+3]).tolist())
    return coord_eck

def project(hess, grad, Natom, coord):
    # remove translational and rotational components
    # translational
    mass, massmatrix=get_masses()
    trans=[np.zeros(3*Natom), np.zeros(3*Natom), np.zeros(3*Natom)]
    for i in range(3*Natom):
        if i%3 == 0:
            trans[0][i]=math.sqrt(mass[i//3])
        elif i%3 == 1:
            trans[1][i]=math.sqrt(mass[i//3])
        elif i%3 == 2:
            trans[2][i]=math.sqrt(mass[i//3])

    # rotational
    # calculate the Inertia matrix
    I=np.zeros((3, 3))
    I[0][0]=sum(mass[i]*(coord[i*3+1]**2+coord[i*3+2]**2) for i in range(Natom))
    I[1][1]=sum(mass[i]*(coord[i*3]**2+coord[i*3+2]**2) for i in range(Natom))
    I[2][2]=sum(mass[i]*(coord[i*3]**2+coord[i*3+1]**2) for i in range(Natom))
    I[1][0]=-sum(mass[i]*coord[i*3]*coord[i*3+1] for i in range(Natom))
    I[2][0]=-sum(mass[i]*coord[i*3]*coord[i*3+2] for i in range(Natom))
    I[2][1]=-sum(mass[i]*coord[i*3+1]*coord[i*3+2] for i in range(Natom))
    I[0][1]=-sum(mass[i]*coord[i*3]*coord[i*3+1] for i in range(Natom))
    I[0][2]=-sum(mass[i]*coord[i*3]*coord[i*3+2] for i in range(Natom))
    I[1][2]=-sum(mass[i]*coord[i*3+1]*coord[i*3+2] for i in range(Natom))

    Ival, Ivec = np.linalg.eigh(I)

    # calculate the rotation
    rot=[np.zeros(3*Natom), np.zeros(3*Natom), np.zeros(3*Natom)]
    for i in range(3*Natom):
        if i%3 == 0:
            rot[0][i]=np.cross(Ivec[:, 0], [coord[i], coord[i+1], coord[i+2]])[0]*math.sqrt(mass[i//3])
            rot[1][i]=np.cross(Ivec[:, 1], [coord[i], coord[i+1], coord[i+2]])[0]*math.sqrt(mass[i//3])
            rot[2][i]=np.cross(Ivec[:, 2], [coord[i], coord[i+1], coord[i+2]])[0]*math.sqrt(mass[i//3])
        elif i%3 == 1:
            rot[0][i]=np.cross(Ivec[:, 0], [coord[i-1], coord[i], coord[i+1]])[1]*math.sqrt(mass[i//3])
            rot[1][i]=np.cross(Ivec[:, 1], [coord[i-1], coord[i], coord[i+1]])[1]*math.sqrt(mass[i//3])
            rot[2][i]=np.cross(Ivec[:, 2], [coord[i-1], coord[i], coord[i+1]])[1]*math.sqrt(mass[i//3])
        elif i%3 == 2:
            rot[0][i]=np.cross(Ivec[:, 0], [coord[i-2], coord[i-1], coord[i]])[2]*math.sqrt(mass[i//3])
            rot[1][i]=np.cross(Ivec[:, 1], [coord[i-2], coord[i-1], coord[i]])[2]*math.sqrt(mass[i//3])
            rot[2][i]=np.cross(Ivec[:, 2], [coord[i-2], coord[i-1], coord[i]])[2]*math.sqrt(mass[i//3])

    # orthogonalise the vectors
    A=np.transpose(linalg.orth(np.array([trans[0], trans[1], trans[2], rot[0], rot[1], rot[2]]).T))

    # calculate the projection matrix and symmetrise (shouldn't be necessary)
    P=np.identity(3*Natom) - np.outer(A[0], A[0]) - np.outer(A[1], A[1]) - np.outer(A[2], A[2]) - np.outer(A[3], A[3]) - np.outer(A[4], A[4]) - np.outer(A[5], A[5])
    symm_P=np.divide(np.add(P, np.transpose(P)), 2)

    # massweight the Hessian, project and symmetrise
    temp = np.matmul(hess, np.linalg.inv(massmatrix))
    mm_hess = np.matmul(np.transpose(np.linalg.inv(massmatrix)), temp)

    temp=np.matmul(mm_hess, symm_P)
    proj_hess=np.matmul(np.transpose(symm_P), temp)
    proj_hess=np.divide(np.add(proj_hess, np.transpose(proj_hess)), 2)

    temp=np.matmul(proj_hess, massmatrix)
    w_proj_hess=np.matmul(np.transpose(massmatrix), temp)

    # massweight and project the gradient
    w_proj_grad=np.matmul(symm_P, grad)
#    temp = np.matmul(grad[i], np.linalg.inv(massmatrix))
#    mm_grad[i] = np.matmul(np.transpose(np.linalg.inv(massmatrix)), temp)
#    temp=np.matmul(mm_grad[i], symm_P)
#    proj_grad[i]=np.matmul(np.transpose(symm_P), temp)
#    temp=np.matmul(proj_grad[i], massmatrix)
#    w_proj_grad[i]=np.matmul(np.transpose(massmatrix), temp)
    return(w_proj_hess, w_proj_grad)

def check_hess(hessian):
    # Check if Hessian is positive definite
    if not np.all(np.linalg.eigvals(hessian) > 0):
        print('fix hessian')
        identity = np.eye(len(hessian))
        eps = 1e-8
        hes_reg = hessian + eps * identity
        eps_max = 100.0
        while not np.all(np.linalg.eigvals(hes_reg) > 0) and eps <= eps_max:
            eps *= 10.0
            hes_reg = hessian + eps * identity
        if eps > eps_max:
            print(hes_reg)
            print(is_pos_def(hes_reg))
            raise ValueError("Failed to regularize Hessian!")
        return hes_reg
    else:
        return hessian

def filter_hess(proj_hess, proj_grad):
    # Calculation the step without rotational and translational components in the Hessian
    eigvals, eigvecs = np.linalg.eigh(proj_hess)
    reduced_eigvecs=copy.deepcopy(eigvecs)
    red_eigvals=copy.deepcopy(eigvals)
    s=0
    for i in range(len(eigvals)):
        if abs(eigvals[i]) < 0.00000001:
            reduced_eigvecs=np.delete(reduced_eigvecs, i-s, 1)
            red_eigvals=np.delete(red_eigvals, i-s)
            s+=1
    temp=np.matmul(proj_grad, reduced_eigvecs)
    temp2=np.matmul(np.linalg.inv(np.diag(red_eigvals)), temp)
    step = np.matmul(temp2, np.transpose(reduced_eigvecs))
    return(step)

def fit_quartic(y0, y1, g0, g1):
    # Copied from pyBerny
    """Fit constrained quartic polynomial to function values and derivatives at x = 0,1.
    Returns position and function value of minimum or None if fit fails or has
    a maximum. Quartic polynomial is constrained such that it's 2nd derivative
    is zero at just one point. This ensures that it has just one local
    extremum.  No such or two such quartic polynomials always exist. From the
    two, the one with lower minimum is chosen.
    """

    def g(y0, y1, g0, g1, c):
        a = c + 3 * (y0 - y1) + 2 * g0 + g1
        b = -2 * c - 4 * (y0 - y1) - 3 * g0 - g1
        return np.array([a, b, c, g0, y0])
    def quart_min(p):
        r = np.roots(np.polyder(p))
        is_real = np.isreal(r)
        if is_real.sum() == 1:
            minim = r[is_real][0].real
        else:
            minim = r[(r == max(-abs(r))) | (r == -max(-abs(r)))][0].real
        return minim, np.polyval(p, minim)

    # discriminant of d^2y/dx^2=0
    D = -((g0 + g1) ** 2) - 2 * g0 * g1 + 6 * (y1 - y0) * (g0 + g1) - 6 * (y1 - y0) ** 2
    if D < 1e-11:
        return None, None
    else:
        m = -5 * g0 - g1 - 6 * y0 + 6 * y1
        p1 = g(y0, y1, g0, g1, 0.5 * (m + np.sqrt(2 * D)))
        p2 = g(y0, y1, g0, g1, 0.5 * (m - np.sqrt(2 * D)))
        if p1[0] < 0 and p2[0] < 0:
            return None, None
        [minim1, minval1] = quart_min(p1)
        [minim2, minval2] = quart_min(p2)
        if minval1 < minval2:
            return minim1, minval1
        else:
            return minim2, minval2


def fit_cubic(y0, y1, g0, g1):
    # copied from pyBerny
    """Fit cubic polynomial to function values and derivatives at x = 0, 1.
    Returns position and function value of minimum if fit succeeds. Fit does
    not succeeds if
    1. polynomial doesn't have extrema or
    2. maximum is from (0,1) or
    3. maximum is closer to 0.5 than minimum
    """
    a = 2 * (y0 - y1) + g0 + g1
    b = -3 * (y0 - y1) - 2 * g0 - g1
    p = np.array([a, b, g0, y0])
    r = np.roots(np.polyder(p))
    if not np.isreal(r).all():
        return None, None
    r = sorted(x.real for x in r)
    if p[0] > 0:
        maxim, minim = r
    else:
        minim, maxim = r
    if 0 < maxim < 1 and abs(minim - 0.5) > abs(maxim - 0.5):
        return None, None
    return minim, np.polyval(p, minim)

def linesearch(coord_opt, coord, new_step, E_opt, energy, grad_opt, grad):
    # based on pyBerny
    dq = np.subtract(coord_opt, coord)
    t, E = fit_quartic(energy, E_opt, np.dot(grad, dq), np.dot(grad_opt, dq))
    print('t from fit', t)
    print('* Interpolated energy:', E)
    if t is None or t < -1 or t > 2:
        t, E = fit_cubic(energy, E_opt, np.dot(grad, dq), np.dot(grad_opt, dq))
        if t is None or t < 0 or t > 1:
            if energy <= E_opt:
                print('* No fit succeeded, staying in new point')
                return 0, energy

            else:
                print('* No fit succeeded, returning to best point')
                return 1, E_opt
        else:
            print('Cubic interpolation was performed')
    else:
        print('Quartic interpolation was performed')
    return t, E

#--------------------------------------------------------------------------------------------------------------------------------#
#                                                                                                                                #
#    Main programme                                                                                                              #
#                                                                                                                                #
#--------------------------------------------------------------------------------------------------------------------------------#

#t_start=time.time()
atom, coord = read_xyz_file()
init_coord_centered=center_molecule(coord)
write_coord_angstrom(atom, init_coord_centered,'init_cent.xyz')
mass, massmatrix=get_masses()
Natom=len(atom)
not_converged=True
iteration=0
while not_converged:
    # Run Gaussian
    prepare_g16_input(coord)
    run_gaussian()
    # Extract the data
    energy, new_grad=read_Gaussian_out()
    newname=project_name+'_'+str(iteration)+'.log'
    shutil.copyfile(project_name+'.log', newname)
    Hess=read_hessian()
#   Hess=check_hess(Hess) 
    # Compute the step
    proj_hess, proj_grad = project(Hess, new_grad, Natom, init_coord_centered)
    new_step = filter_hess(proj_hess, proj_grad)
#    eigvals, eigvecs = np.linalg.eigh(proj_hess)
#    if 0 in eigvals:
#        sys.exit('The Hessian had a 0 eigenvalue.')
#    new_step=np.matmul(np.linalg.inv(Hess), new_grad)
    if iteration == 0:
        new_coord=np.subtract(coord, trust_radius*new_step)
    else:
        t,E = linesearch(coord_opt, coord, new_step, E_opt, energy, grad_opt, new_grad)
        new_coord=np.subtract(coord, t*new_step)

     # Old leapfrog kind of linesearch method
#    if iteration%2==0:
#        new_coord=np.add(coord, trust_radius*new_step)
#    else:
#        p1=-np.dot(step, grad)
#        p2=-np.dot(step, new_grad)
#        p3=-np.dot(new_step, new_grad)
#        if np.sign(p1)==np.sign(p2):
#            if abs(p1) > abs(p2):
#                alpha=trust_radius*min(p1,p2)/(max(p1,p2)+min(p1,p2))
#            else:
#                alpha=-(trust_radius*min(p1,p2)/(max(p1,p2)+min(p1,p2)))
#        else:
#            alpha=(p1/(p1-p2))*trust_radius
#        # if p2 > 0: step too small, if p2 < 0: step too large-reject -> extra/interpolate to 0
#        # if p3 > 0: reverse direction
#        print(iteration, p1, p2, p3, alpha)
#        new_coord=np.add(coord, alpha*step)
    
     # Eckart conditions to remove rotation and translation
#    new_coord_centered=center_molecule(new_coord)
#    eck_coord=solve_eckart(init_coord_centered, new_coord_centered,mass)

    # Check convergence - improve function once optimisation works
    if np.allclose(coord, new_coord):
        not_converged=False

#    # Wrap up and prepare for next step
    write_coord_angstrom(atom, new_coord, 'coord.xyz')
    write_all_coord()
    write_iterfile()
    # Base linesearch on best result
    if iteration == 0 or energy < E_opt:
        coord_opt = copy.deepcopy(coord)
        step_opt = copy.deepcopy(new_step)
        grad_opt = copy.deepcopy(new_grad)
        Hess_opt = copy.deepcopy(Hess)
        E_opt = energy
    coord = copy.deepcopy(new_coord)
    iteration+=1
    
#t_end=time.time()
#print('Total time:', t_end-t_start)

